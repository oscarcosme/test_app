(function(){
	setTimeout(function(){
		// alert('holi');
	}, 5000);

	var group1 = new TimelineMax({paused:true}),
		group2 = new TimelineMax({paused:true}),
		group2_1 = new TimelineMax({paused:true}),
		group3 = new TimelineMax({paused:true}),
		group4 = new TimelineMax({paused:true}),
		freewayEaseTween = new TimelineMax({paused:true, reversed:true});


	//INICIAR ANIMACIONES

	function iniciar(tab){

		switch(tab){
			
			case "home":

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-1'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group1.play();
				    }else if(direction == 'up'){
				    	group1.reverse();
				    }
				  },
				  offset: '80%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-2'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group2.play();
				    	// freewayEaseTween.play();
				    }else if(direction == 'up'){
				    	group2.reverse();
				    	// freewayEaseTween.reversed(true);
				    }
				  },
				  offset: '50%'
				  // offset: 'bottom-in-view'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-2-1'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	//freewayEaseTween.play();
				    }else if(direction == 'up'){
				    	//freewayEaseTween.reversed(true);
				    }
				  },
				  offset: '-10%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-3'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group3.play();
				    }else if(direction == 'up'){
				    	group3.reverse();
				    }
				  },
				  offset: '80%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-4'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group4.play();
				    }else if(direction == 'up'){
				    	group4.reverse();
				    }
				  },
				  offset: '60%'
				});

				break;

			case 'credits':

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-11'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group11.play();
				    	// console.log('1');
				    }else if(direction == 'up'){
				    	group11.reverse();
				    }
				  },
				  offset: '70%'
				});

				break;
		}
	}

	//ANIMANDO
	freewayEaseTween
		.set('.newDay',{backgroundSize:"100% 100%"})
		.to('.newDay', 1.5, {
			backgroundSize: "-=15% -=15%",
			autoRound:false,
			ease: Power1.ease0ut
		});
	/*
	freewayEaseTween2
		.set('.newDay',{backgroundSize:"100% 100%"})
		.to('.newDay', 1.5, {
			backgroundSize: "-=15% -=15%",
			autoRound:false,
			ease: Power1.ease0ut
		});
	freewayEaseTween3
		.set('.newDay',{backgroundSize:"100% 100%"})
		.to('.newDay', 1.5, {
			backgroundSize: "-=10% -=10%",
			autoRound:false,
			ease: Power1.ease0ut
		});
	*/

	group1
		.from(".intro h1", 1, {opacity:0, y:-20, ease:Back.easeOut}, 2)
		.from(".intro h2", 1, {opacity:0, y:20, ease:Back.easeOut}, 2.5);

	group2
		.from(".newDay h1", 0.7, {opacity:0, y:-100, ease:Back.easeOut}, 1)
		.from(".newDay ul li:nth-child(1)", 0.5, {opacity:0, y:20, ease:Back.easeOut}, 2)
		.from(".newDay ul li:nth-child(2)", 0.5, {opacity:0}, 2.25)
		.from(".newDay ul li:nth-child(3)", 0.5, {opacity:0, y:20, ease:Back.easeOut}, 2.5)
		.from(".newDay ul li:nth-child(4)", 0.5, {opacity:0}, 2.75)
		.from(".newDay ul li:nth-child(5)", 0.5, {opacity:0, y:20, ease:Back.easeOut}, 3);
	// group2_1
	group3
		.from(".newDayContent .box.left", 1, {opacity:0, x:-20, ease:Back.easeOut}, 1)
		.from(".newDayContent .gallery01", 1, {opacity:0, y:150, scale:0.8, ease:Back.easeOut}, 1)
		.from(".newDayContent .gallery02", 1, {opacity:0, y:150, scale:0.8, ease:Back.easeOut}, 1.5)
		.from(".newDayContent .gallery03", 1, {opacity:0, y:150, scale:0.8, ease:Back.easeOut}, 2)
		.from(".newDayContent .gallery04", 1, {opacity:0, y:150, scale:0.8, ease:Back.easeOut}, 2.5)
		.from(".newDayContent .gallery05", 1, {opacity:0, y:150, scale:0.8, ease:Back.easeOut}, 3)
		.from(".newDayContent .gallery06", 1, {opacity:0, y:150, scale:0.8, ease:Back.easeOut}, 3.5)
		.from(".newDayContent .gallery07", 1, {opacity:0, y:150, scale:0.8, ease:Back.easeOut}, 4);

	group4
		// .to('.colors .right', 1, {backgroundSize:"+=50% +=50%"}, 1)
		.set('.colors .right',{backgroundSize:"100% 100%"})
		.to('.colors .right', 5, {
			backgroundSize: "+=25% +=25%",
			autoRound: false,
			ease: Power1.ease0ut
		})

		.from(".colors h1", 1, {opacity:0, x:-40, ease:Back.easeOut}, 1)
		.from(".colors ul li ul li:nth-child(1)", 1, {opacity:0, x:-40, scale:0.9, ease:Back.easeOut}, 1.5)
		.from(".colors ul li ul li:nth-child(3)", 1, {opacity:0, x:40, scale:0.9, ease:Back.easeOut}, 1.5);


	// INIT TWEEN
	//iniciar('home');


	// PERSONALIZADOS
	

	//===========================================//
	
	



































})();