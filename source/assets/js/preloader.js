var g_Preloader = new GSPreloader({
	radius:42, 
	dotSize:15, 
	dotCount:10, 
	colors:["#FFFFFF","#33CCCC"],//"purple","#61AC27","#555","#FF6600"
	boxOpacity:0,
	boxBorder:"0",
	animationOffset: 1.8,
});
function GSPreloader(options) {
	options = options || {};
	var parent = options.parent || document.body,
	element = this.element = document.createElement("div"),
	radius = options.radius || 42,
	dotSize = options.dotSize || 15,
	animationOffset = options.animationOffset || 1.8,
	createDot = function(rotation) {
		var dot = document.createElement("div");
		element.appendChild(dot);
		TweenLite.set(dot, {width:dotSize, height:dotSize, transformOrigin:(-radius + "px 0px"), x: radius, backgroundColor:colors[colors.length-1], borderRadius:"50%", force3D:true, position:"absolute", rotation:rotation});
		dot.className = options.dotClass || "preloader-dot";
		return dot; 
	}, 
	i = options.dotCount || 10,
	rotationIncrement = 360 / i,
	colors = options.colors || ["#61AC27","black"],
	animation = new TimelineLite({paused:true}),
	dots = [],
	isActive = false,
	box = document.createElement("div"),
	tl, dot, closingAnimation, j;
	colors.push(colors.shift());

	TweenLite.set(box, {width: radius * 2 + 70, height: radius * 2 + 70, borderRadius:"14px", backgroundColor:options.boxColor || "white", border: options.boxBorder || "1px solid #AAA", position:"absolute", xPercent:-50, yPercent:-50, opacity:((options.boxOpacity !== null) ? options.boxOpacity : 0.3)});
	box.className = options.boxClass || "preloader-box";
	element.appendChild(box);

	parent.appendChild(element);
	TweenLite.set(element, {position:"fixed", top:"45%", left:"50%", perspective:600, overflow:"visible", zIndex:2000});
	animation.from(box, 0.1, {opacity:0, scale:0.1, ease:Power1.easeOut}, animationOffset);
	while (--i > -1) {
		dot = createDot(i * rotationIncrement);
		dots.unshift(dot);
		animation.from(dot, 0.1, {scale:0.01, opacity:0, ease:Power1.easeOut}, animationOffset);
		tl = new TimelineMax({repeat:-1, repeatDelay:0.25});
		for (j = 0; j < colors.length; j++) {
			tl.to(dot, 2.5, {rotation:"-=360", ease:Power2.easeInOut}, j * 2.9).to(dot, 1.2, {skewX:"+=360", backgroundColor:colors[j], ease:Power2.easeInOut}, 1.6 + 2.9 * j);
		}
		animation.add(tl, i * 0.07);
	}
	if (TweenLite.render) {
		TweenLite.render();
	}

	this.active = function(show) {
		if (!arguments.length) {
			return isActive;
		}
		if (isActive != show) {
			isActive = show;
			if (closingAnimation) {
				closingAnimation.kill();
			}
			if (isActive) {
				$("#idDivFondoTranparente").removeAttr('style').show();
				element.style.visibility = "visible";
				TweenLite.set([element, box], {rotation:0});
				animation.play(animationOffset);
			} else {
				closingAnimation = new TimelineLite();
				if (animation.time() < animationOffset + 0.3) {
					animation.pause();
					closingAnimation.to(element, 1, {rotation:-360, ease:Power1.easeInOut}).to(box, 1, {rotation:360, ease:Power1.easeInOut}, 0);
					
				}
				closingAnimation.staggerTo(dots, 0.3, {scale:0.01, opacity:0, ease:Power1.easeIn, overwrite:false}, 0.05, 0)
				.to(box, 0.4, {opacity:0, scale:0.2, ease:Power2.easeIn, overwrite:false}, 0)
				.to("#idDivFondoTranparente", 1, {opacity:0, ease:Power2.easeIn, overwrite:false}, 0)
				.to("#idPreloader", 1, {opacity:0, ease:Power2.easeIn, overwrite:false}, 0)
				.call(function() { 
					animation.pause();
					closingAnimation = null; 
					$("#idDivFondoTranparente").removeAttr('style').hide();
					$("#idPreloader").removeAttr('style').hide();
				}).set(element, {visibility:"hidden"});
			}
		}
		return this;
	};
}